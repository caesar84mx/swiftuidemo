//
//  ContentView.swift
//  SwiftUIDemo
//
//  Created by Maxim Dymnov on 6/18/20.
//  Copyright © 2020 Maxim Dymnov. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        FeedbackView()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
