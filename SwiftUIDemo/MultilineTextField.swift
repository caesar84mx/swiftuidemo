//
//  MultilineTextField.swift
//  SwiftUIDemo
//
//  Created by Maxim Dymnov on 6/18/20.
//  Copyright © 2020 Maxim Dymnov. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI

struct MultilineTextField: View {
    @Binding private var text: String
    @State private var viewHeight: CGFloat = 40

    private var placeholder: String

    private var internalText: Binding<String> {
        Binding<String>(get: { self.text }) { //swiftlint:disable:this multiple_closures_with_trailing_closure
            self.text = $0
        }
    }

    var body: some View {
        UITextViewWrapper(text: self.internalText, calculatedHeight: $viewHeight)
            .frame(minHeight: viewHeight, maxHeight: viewHeight)
            .background(placeholderView, alignment: .topLeading)
    }

    var placeholderView: some View {
        Group {
            if $text.wrappedValue.isEmpty {
                Text(placeholder).foregroundColor(.gray)
                .padding(.leading, 4)
                .padding(.top, 8)
            }
        }
    }

    init (_ placeholder: String = "", text: Binding<String>) {
        self.placeholder = placeholder
        self._text = text
    }
}

private struct UITextViewWrapper: UIViewRepresentable {
    typealias UIViewType = UITextView

    @Binding var text: String
    @Binding var calculatedHeight: CGFloat

    func makeUIView(context: UIViewRepresentableContext<UITextViewWrapper>) -> UITextView {
        let textField = UITextView()
        textField.endEditing(true)
        textField.delegate = context.coordinator
        textField.isEditable = true
        textField.font = UIFont.preferredFont(forTextStyle: .body)
        textField.isSelectable = true
        textField.isUserInteractionEnabled = true
        textField.isScrollEnabled = false
        textField.backgroundColor = UIColor.clear
        textField.returnKeyType = .done

        textField.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)

        return textField
    }

    func updateUIView(_ uiView: UITextView, context: UIViewRepresentableContext<UITextViewWrapper>) {
        if uiView.text != self.text {
            uiView.text = self.text
        }

        UITextViewWrapper.recalculateHeight(view: uiView, result: $calculatedHeight)
    }

    func makeCoordinator() -> Coordinator {
        Coordinator(text: $text, height: $calculatedHeight)
    }

    static func recalculateHeight(view: UIView, result: Binding<CGFloat>) {
        let newSize = view.sizeThatFits(CGSize(width: view.frame.size.width, height: CGFloat.greatestFiniteMagnitude))
        if result.wrappedValue != newSize.height {
            DispatchQueue.main.async {
                result.wrappedValue = newSize.height // call in next render cycle.
            }
        }
    }
}

final class Coordinator: NSObject, UITextViewDelegate {
    var text: Binding<String>
    var calculatedHeight: Binding<CGFloat>

    init(text: Binding<String>, height: Binding<CGFloat>, onDone: (() -> Void)? = nil) {
        self.text = text
        self.calculatedHeight = height
    }

    func textViewDidChange(_ uiView: UITextView) {
        text.wrappedValue = uiView.text
        UITextViewWrapper.recalculateHeight(view: uiView, result: calculatedHeight)
    }

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.endEditing(true)
            return false
        }
        return true
    }
}
