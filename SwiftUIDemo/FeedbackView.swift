//
//  FeedbackView.swift
//  SwiftUIDemo
//
//  Created by Maxim Dymnov on 6/18/20.
//  Copyright © 2020 Maxim Dymnov. All rights reserved.
//

import SwiftUI
import Combine

struct FeedbackView: View {
    @ObservedObject private var viewModel = ViewModel()
    @State private var isPopupShown = false
    @State private var popupMessage = ""

    var body: some View {
        Group {
            ScrollView {
                VStack(alignment: .leading, spacing: 20) {
                    HStack {
                        Spacer()
                        Text("Feedback")
                            .fontWeight(.bold)
                            .font(.largeTitle)
                        Spacer()
                    }

                    VStack(alignment: .leading) {
                        Text("First Name")
                            .fontWeight(.bold)
                            .font(.subheadline)
                            .padding([.leading], 10)
                        TextField("Enter your first name", text: $viewModel.firstName)
                            .textFieldStyle(RoundedBorderTextFieldStyle())
                    }

                    VStack(alignment: .leading) {
                        Text("Last Name")
                            .fontWeight(.bold)
                            .font(.subheadline)
                            .padding([.leading], 10)
                        TextField("Enter your last name", text: $viewModel.lastName)
                            .textFieldStyle(RoundedBorderTextFieldStyle())
                    }

                    VStack(alignment: .leading) {
                        Text("Email")
                            .fontWeight(.bold)
                            .font(.subheadline)
                            .padding([.leading], 10)
                        TextField("Enter your email", text: $viewModel.email)
                            .textFieldStyle(RoundedBorderTextFieldStyle())
                    }

                    VStack(alignment: .leading) {
                        Text("Subject")
                            .fontWeight(.bold)
                            .font(.subheadline)
                            .padding([.leading], 10)
                        TextField("What is the subject of your feedback?", text: $viewModel.subject)
                            .textFieldStyle(RoundedBorderTextFieldStyle())
                    }

                    VStack(alignment: .leading) {
                        Text("Feedback")
                            .fontWeight(.bold)
                            .font(.subheadline)
                            .padding([.leading], 10)
                        MultilineTextField("Leave your feedback here", text: $viewModel.feedback)
                        .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.gray))
                    }

                    HStack {
                        Spacer()
                        Button(action: {
                            self.viewModel.sendFeedback { message in
                                self.showPopup(with: message)
                            }
                        }) {
                            Text("Submit")
                        }
                        Spacer()
                    }
                }
                .padding([.vertical, .horizontal], 20)
            }
            .keyboardAdaptive()

            self.getPopup(with: popupMessage)
        }
    }

    private func showPopup(with message: String) {
        if !$isPopupShown.wrappedValue {
            self.isPopupShown = true
            self.popupMessage = message
        }
    }

    private func getPopup(with message: String) -> AnyView {
        if $isPopupShown.wrappedValue {
            return AnyView(
                VStack(spacing: 10) {
                    Text("Success :)")
                        .font(.headline)
                    Text(message)
                        .font(.body)
                    Button(action: { self.isPopupShown = false }) {
                        Text("Ok")
                    }
                }
                .padding([.bottom], 10)
            )
        } else {
            return AnyView(EmptyView())
        }
    }
}

extension FeedbackView {
    class ViewModel: ObservableObject {
        var firstName: String = "" {
            willSet { objectWillChange.send() }
        }

        var lastName: String = "" {
            willSet { objectWillChange.send() }
        }

        var email: String = "" {
            willSet { objectWillChange.send() }
        }

        var subject: String = "" {
            willSet { objectWillChange.send() }
        }

        var feedback: String = "" {
            willSet { objectWillChange.send() }
        }

        func sendFeedback(onResult: (String) -> Void) {
            onResult("Feedback sent successfully")
        }
    }
}

struct FeedbackView_Preview: PreviewProvider {
    static var previews: some View {
        FeedbackView()
    }
}

